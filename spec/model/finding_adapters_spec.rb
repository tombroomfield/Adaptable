describe Adaptable::Model do
  let(:model) { Adaptable::FakeModel }
  describe '#adapter_for' do
    context 'When just using defaults' do
      it 'should return the correct default' do
        expect(model.adapter_for(action: :create, format: :html))
          .to eq Adapters::Adaptable::FakeModels::Html::CreateAdapter
      end
    end

    context 'When changing the format' do
      it 'should return the correct default' do
        expect(model.adapter_for(action: :create, format: :json))
          .to eq Adapters::Adaptable::FakeModels::Json::CreateAdapter
      end
    end

    context 'When changing the action' do
      it 'should return the correct default' do
        expect(model.adapter_for(action: :show, format: :html))
          .to eq Adapters::Adaptable::FakeModels::Html::ShowAdapter
      end
    end

    context 'When there is no adapter' do
      it 'should throw the correct error' do
        expect do
          model.adapter_for(action: :create, format: :xml)
        end.to raise_error Adaptable::MissingAdapterError
      end
    end
  end
end
