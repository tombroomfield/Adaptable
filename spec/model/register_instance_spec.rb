describe 'Instances' do
  let(:model) { Adaptable::FakeModel.new }
  let(:adapter) { Adaptable::FakeAdapter }

  describe '.register_adapter' do
    context 'When I register an adapter' do
      context 'Without a format' do
        before do
          model.register_adapter(adapter, action: :create, format: :html)
        end

        it 'should be added to adapters as html' do
          expect(model.adapter_for(action: :create, format: :html))
            .to eq adapter
        end
      end

      context 'With a format supplied' do
        before do
          model.register_adapter(adapter, action: :create, format: :json)
        end

        it 'should be added to adapters as json' do
          expect(model.adapter_for(action: :create, format: :json))
            .to eq adapter
        end
      end
    end

    context 'When set on the class' do
      before do
        model.class.register_adapter(adapter, action: :create, format: :html)
      end

      it 'should return the class one' do
        expect(model.adapter_for(action: :create, format: :html)).to eq adapter
      end
    end

    context 'When overriding a class' do
      let(:adapter2) { Adaptable::FakeAdapterTwo }

      before do
        model.class.register_adapter(adapter, action: :create, format: :html)
        model.register_adapter(adapter2, action: :create, format: :html)
      end

      it 'should return the instance one' do
        expect(model.adapter_for(action: :create, format: :html)).to eq adapter2
      end
    end
  end
end
