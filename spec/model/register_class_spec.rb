describe 'Classes' do
  let(:model) { Adaptable::FakeModel }
  let(:adapter) { Adaptable::FakeAdapter }

  describe '.register_adapter' do
    context 'When I register an adapter' do
      context 'Without a format' do
        before do
          model.register_adapter(adapter, action: :create, format: :html)
        end

        it 'should be added to adapters as html' do
          expect(model.adapter_for(action: :create, format: :html))
            .to eq adapter
        end
      end

      context 'With a format supplied' do
        before do
          model.register_adapter(adapter, action: :create, format: :json)
        end

        it 'should be added to adapters as json' do
          expect(model.adapter_for(action: :create, format: :json))
            .to eq adapter
        end
      end
    end
  end
end
