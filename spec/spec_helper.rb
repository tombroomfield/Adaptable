require 'bundler/setup'
require 'adaptable'
require 'byebug'

require_relative 'fakes/fake_model'
require_relative 'fakes/fake_adapter'
require_relative 'fakes/fake_adapter_two'
require_relative 'fakes/fake_controller'
require_relative 'adapters/adaptable/fake_models/html/create_adapter'
require_relative 'adapters/adaptable/fake_models/html/show_adapter'
require_relative 'adapters/adaptable/fake_models/json/create_adapter'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
