describe Adaptable::Controller do
  describe '#current_adapter' do
    let(:controller) { Adaptable::FakeController.new }
    let(:model) { Adaptable::FakeModel }

    it 'should return the correct one' do
      expect(controller.current_adapter(model).class)
        .to eq Adapters::Adaptable::FakeModels::Html::CreateAdapter
    end

    it 'should have set the controller variable' do
      expect(controller.current_adapter(model).controller).to eq controller
    end

    it 'should delegate to controller' do
      expect(controller.current_adapter(model).action_name).to eq 'create'
    end
  end
end
