module Adaptable
  class FakeController
    include Adaptable::Controller

    def action_name
      'create'
    end

    def request
      Struct.new(:format).new(Struct.new(:symbol).new(:html))
    end
  end
end
