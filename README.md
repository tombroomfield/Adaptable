# Adaptable

Adaptable is a minimal pattern that provides dedicated objects to decide what do to at the end of a controller action. This helps your controller actions to stay small, clean and properly decoupled from your model layer.

An adapter is a dedicated class responsible for deciding what to do based on the success / failure of a specific controller action, for a specific format.

For instance:

### Before using Adaptable:

``` ruby
class UserController < ApplicationController
  def create
    if @user.save
      respond_to do |format|
        format.html do
          redirect_to user_path(@user)
        end
        format.json do
          render json: { user: @user }
        end
      end
    else
      respond_to do |format|
        format.html do
          render :new
        end
        format.json do
          render json: { user: @user.errors }
        end
      end
    end
  end
  ...
end
```

That's a big method, which will only grow as our app does.

### After using Adaptable:

``` ruby
class UserController < ApplicationController
  def create
    if @user.save
      current_adapter(@user).success
    else
      current_adapter(@user).failure
    end
  end
end
```

The adapters:

``` ruby
class Adapters::Users::Html::CreateAdapter < Adaptable::Adapter
  def success
    redirect_to user_path(resource)
  end

  def failure
    render :new
  end
end
```

``` ruby
class Adapters::Users::Json::CreateAdapter < Adaptable::Adapter
  def success
    render json: resource
  end

  def failure
    render json: resource.errors
  end
end
```

This has the following benefits:
* Far simpler controller actions.
* Add behaviour for other formats (CSV, JSON) without modifying your controller.
* Small, easily testable adapter classes.
* Encourages better REST practices.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'adaptable'
```

And then execute:

    $ bundle

## Setup
Include the controller module in your application controller:

``` ruby
include Adaptable::Controller
```

And then, in either you application record or each model using Adaptable:

``` ruby
include Adaptable::Model
```

By default, adapters will follow the following pattern:

For `User`, in the `create` action of controller responding to a HTML request:
`models/adapters/users/html/create_adapter.rb`

## Customising adapters

If you wish to define different adapters to respond, you can customise them at either the class or the instance level:

``` ruby
User.register_adaptable(MyCustomAdater, format: :html, action: :create)
```

To dynamically modify the adaptable for an individual object:

``` ruby
user.register_adaptable(MyCustomAdater, format: :html, action: :create)
```

## Contributing

https://gitlab.com/tombroomfield/Adaptable
