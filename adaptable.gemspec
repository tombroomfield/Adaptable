# coding: utf-8

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'adaptable/version'

Gem::Specification.new do |spec|
  spec.name = 'adaptable'
  spec.version = Adaptable::VERSION
  spec.authors = ['Thomas Broomfield']
  spec.email = ['tomplbroomfield@gmail.com']

  spec.summary = 'Tame your Rails controllers with the adaptable model.'
  spec.homepage = 'https://gitlab.com/tombroomfield/adaptable'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'activesupport'

  spec.add_development_dependency 'bundler', '~> 1.14'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'byebug'
end
