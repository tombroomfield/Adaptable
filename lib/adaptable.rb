require 'adaptable/version'

require 'active_support/all'

require 'adaptable/model'
require 'adaptable/controller'
require 'adaptable/adapter'
require 'adaptable/finder'
require 'adaptable/safe_hash'
require 'adaptable/missing_adapter_error'

module Adaptable
end
