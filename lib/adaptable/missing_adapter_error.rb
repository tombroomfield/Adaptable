module Adaptable
  class MissingAdapterError < StandardError
    def initialize(klass_name)
      super("Missing adapter: #{klass_name}")
    end
  end
end
