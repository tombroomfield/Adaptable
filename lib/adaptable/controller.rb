module Adaptable
  module Controller
    extend ActiveSupport::Concern

    def current_adapter(resource)
      Finder.new(resource, action_name, request.format.symbol).resolve.new(self)
    end
  end
end
