module Adaptable
  class SafeHash
    class << self
      def build
        Hash.new { |h, k| h[k] = h.class.new(&h.default_proc) }
      end
    end
  end
end
