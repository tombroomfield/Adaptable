module Adaptable
  class Adapter < SimpleDelegator
    def controller
      __getobj__
    end
  end
end
