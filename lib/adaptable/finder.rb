module Adaptable
  class Finder
    attr_accessor :resource, :action, :request_format

    def initialize(resource, action, format)
      self.resource = resource
      self.action = action
      self.request_format = format
    end

    def resolve
      resource.adapters = SafeHash.build unless resource.adapters.present?
      from_specific_action.presence || from_default.constantize
    rescue NameError
      raise MissingAdapterError.new(from_default)
    end

    private

    def from_specific_action
      resource.adapters.dig(request_format, action).presence || from_class
    end

    def from_default
      "Adapters::#{class_name.pluralize}::#{format_name}::#{action_name}Adapter"
    end

    def from_class
      return false if resource.is_a?(Class)
      resource.class.adapters.dig(request_format, action)
    end

    def class_name
      return resource.name if resource.is_a?(Class)
      resource.class.name
    end

    def action_name
      action.capitalize
    end

    def format_name
      request_format.capitalize
    end
  end
end
