module Adaptable
  module Model
    extend ActiveSupport::Concern
    included do
      attr_accessor :adapters
      self.adapters = SafeHash.build
    end

    def register_adapter(adapter, **opts)
      self.adapters = SafeHash.build unless adapters.present?
      request_format = opts.fetch(:format)
      action = opts.fetch(:action)
      adapters[request_format][action] = adapter
    end

    def adapter_for(**opts)
      self.class.adapter_for(opts.merge(resource: self))
    end

    class_methods do
      attr_accessor :adapters

      def register_adapter(adapter, **opts)
        request_format = opts.fetch(:format)
        action = opts.fetch(:action)
        adapters[request_format][action] = adapter
      end

      def adapter_for(**opts)
        Finder.new(opts.fetch(:resource, self), opts.fetch(:action),
                   opts.fetch(:format)).resolve
      end
    end
  end
end
